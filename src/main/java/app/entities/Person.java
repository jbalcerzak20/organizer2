package app.entities;


import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@Table(name = "person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String login;
    private String password;
    private String email;
    private Date createdAt = new Date(System.currentTimeMillis());
    private Date updatedAt = new Date(System.currentTimeMillis());
    private String temporaryPassword;
    private Boolean passwordToChange;
    @OneToMany(mappedBy = "person",fetch = FetchType.EAGER)
    @OrderBy(value = "login desc")
    private List<LogPerson> loggers;
}

