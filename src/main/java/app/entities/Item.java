package app.entities;

import app.services.SecureService;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String login;
    private String password;
    private String description;
    private String link;
    private String email;
    private Date createdAt = new Date(System.currentTimeMillis());
    private Date updatedAt = new Date(System.currentTimeMillis());

    @Transient
    private String loginDecode;
    @Transient
    private String passwordDecode;

    public void initDecodeText(SecureService secureService){
        loginDecode = secureService.decodeAES(login);
        passwordDecode = secureService.decodeAES(password);
    }
}
