package app.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
public class LogPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Timestamp login = new Timestamp(System.currentTimeMillis());
    private Timestamp logout;
    @ManyToOne
    private Person person;
}
