package app.controllers;

import app.domain.MailObject;
import app.entities.LogPerson;
import app.entities.Person;
import app.repositories.LoggingHistoryRepository;
import app.repositories.PersonRepository;
import app.services.EmailService;
import app.services.SecureService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lombok.Getter;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ResourceBundle;

@Controller
public class MainController implements Initializable {

    @FXML
    private ScrollPane root;
    @FXML
    private TextField login;
    @FXML
    private TextField emailToReset;
    @FXML
    private PasswordField password;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private LoggingHistoryRepository loggingHistoryRepository;
    @Autowired
    private ContentController contentController;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private EmailService emailService;
    @Getter
    private Long currentUserId;
    @Autowired
    private SecureService secureService;

    @Value("${spring.mail.username}")
    private String ownEmail;

    public void onClick() throws IOException, NoSuchAlgorithmException {
        root.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST,event -> {
            LogPerson log = loggingHistoryRepository.findTop1ByLogoutIsNull();
            log.setLogout(new Timestamp(System.currentTimeMillis()));
            loggingHistoryRepository.save(log);
        });

        String login = this.login.getText();
        String password = this.password.getText();
        Person foundPerson = personRepository.findByLogin(login);
        if(foundPerson == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Login lub hasło niepoprawne");
            alert.showAndWait();
            return;
        }

        if (foundPerson.getPasswordToChange()) {
            if (checkPassword(foundPerson.getTemporaryPassword(), password)) {
                foundPerson.setPassword(encodePassword(password));
                foundPerson.setPasswordToChange(false);
                foundPerson.setTemporaryPassword(null);
                foundPerson.setUpdatedAt(new Date(System.currentTimeMillis()));
                personRepository.save(foundPerson);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/content.fxml"));
                this.currentUserId = foundPerson.getId();
                loader.setControllerFactory(applicationContext::getBean);
                Parent node = loader.load();
                Stage primary = (Stage) root.getScene().getWindow();
                primary.setScene(new Scene(node, 600, 400));
                primary.setMaximized(true);
                addLoggingHistoryRecord(foundPerson);
            }
            else {
                foundPerson.setPasswordToChange(false);
                foundPerson.setTemporaryPassword(null);
                foundPerson.setUpdatedAt(new Date(System.currentTimeMillis()));
                personRepository.save(foundPerson);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Login lub hasło niepoprawne");
                alert.showAndWait();
            }
        } else if (checkPassword(foundPerson != null ? foundPerson.getPassword() : "", password)) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/content.fxml"));
            this.currentUserId = foundPerson.getId();
            loader.setControllerFactory(applicationContext::getBean);
            Parent node = loader.load();
            Stage primary = (Stage) root.getScene().getWindow();
            primary.setScene(new Scene(node, 600, 400));
            primary.setMaximized(true);
            addLoggingHistoryRecord(foundPerson);
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Login lub hasło niepoprawne");
            alert.showAndWait();
        }
    }

    private boolean checkPassword(String fromDB, String fromGUI) throws NoSuchAlgorithmException {
        return fromDB.equalsIgnoreCase(encodePassword(fromGUI));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private String encodePassword(String toEncode) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(toEncode.getBytes());
        byte[] digest = md5.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    @FXML
    private void onPasswordReset() throws NoSuchAlgorithmException {
        Person byEmail = personRepository.findByEmail(emailToReset.getText());
        if (byEmail != null && byEmail.getEmail().equals(emailToReset.getText())) {
            String newPass = RandomStringUtils.randomAlphanumeric(10);
            String encodedNewPass = encodePassword(newPass);
            byEmail.setUpdatedAt(new Date(System.currentTimeMillis()));
            byEmail.setTemporaryPassword(encodedNewPass);
            byEmail.setPasswordToChange(true);
            personRepository.save(byEmail);
            MailObject mail = MailObject.builder()
                    .content("New password: " + newPass)
                    .to(byEmail.getEmail())
                    .from(ownEmail)
                    .subject("Change password")
                    .build();
            emailService.sendEmail(mail);
        }
    }

    private void addLoggingHistoryRecord(Person person){
        LogPerson logPerson = new LogPerson();
        logPerson.setPerson(person);
        loggingHistoryRepository.save(logPerson);
    }
}
