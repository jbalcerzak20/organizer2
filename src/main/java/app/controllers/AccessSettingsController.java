package app.controllers;

import app.entities.Person;
import app.repositories.PersonRepository;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import javax.xml.bind.DatatypeConverter;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.Optional;
import java.util.ResourceBundle;

@Controller
public class AccessSettingsController implements Initializable {

    @Autowired
    private PersonRepository personRepository;
    @FXML
    private TextField login;
    @FXML
    private PasswordField password;
    @FXML
    private TextField email;
    @Autowired
    private MainController mainController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Long currentUserId = mainController.getCurrentUserId();
        Person person = personRepository.findOne(currentUserId);
        login.setText(person.getLogin());
        login.setEditable(false);
        email.setText(person.getEmail());
    }

    @FXML
    private void onChangePassword() throws NoSuchAlgorithmException {

        if(StringUtils.isEmpty(password.getText()))
            return;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potwierdz");
        alert.setContentText("Czy na pewno zmienić hasło?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK){
            Person currentPerson = personRepository.findOne(mainController.getCurrentUserId());
            currentPerson.setPassword(encodePassword(password.getText()));
            currentPerson.setUpdatedAt(new Date(System.currentTimeMillis()));
            personRepository.save(currentPerson);
        }
    }

    @FXML
    private void onChangeEmail() throws NoSuchAlgorithmException {

        if(StringUtils.isEmpty(email.getText()))
            return;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potwierdz");
        alert.setContentText("Czy na pewno zmienić email?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK){
            Person currentPerson = personRepository.findOne(mainController.getCurrentUserId());
            currentPerson.setEmail(email.getText());
            currentPerson.setUpdatedAt(new Date(System.currentTimeMillis()));
            personRepository.save(currentPerson);
        }
    }

    private String encodePassword(String toEncode) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(toEncode.getBytes());
        byte[] digest = md5.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }
}
