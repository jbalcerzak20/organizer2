package app.controllers;

import app.entities.Item;
import app.entities.LogPerson;
import app.repositories.ItemRepository;
import app.repositories.LoggingHistoryRepository;
import app.repositories.PersonRepository;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ResourceBundle;

@Controller
public class ContentController implements Initializable {

    @FXML
    private BorderPane root;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private LoggingHistoryRepository loggingHistoryRepository;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private ItemsController itemsController;
    private Item currentProgressItem;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            onItems();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    public void onCloseApp() {
        LogPerson log = loggingHistoryRepository.findTop1ByLogoutIsNull();
        log.setLogout(new Timestamp(System.currentTimeMillis()));
        loggingHistoryRepository.save(log);
        Platform.exit();
    }

    @FXML
    public void onLoadFromFile() throws IOException, ParseException {
        Stage stage = (Stage) root.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Wybierz plik");
        File file = fileChooser.showOpenDialog(stage);

        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader(file);
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        JSONArray jsonArray = (JSONArray) jsonObject.get("data");
        jsonArray.forEach(it -> {
            JSONObject json = ((JSONObject) it);
            Item item = new Item();
            item.setLogin((String) json.get("login"));
            item.setPassword((String) json.get("password"));
            item.setDescription((String) json.get("desc"));
            item.setEmail((String) json.get("email"));
            item.setLink((String) json.get("page"));
            itemRepository.save(item);
        });
        reader.close();
        itemRepository.findAllByOrderByLogin().forEach(it -> itemsController.getTable().getItems().add(it));
    }


    @FXML
    private void onAccessSetting() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/accessSettings.fxml"));
        loader.setControllerFactory(applicationContext::getBean);
        Parent node = loader.load();
        root.setCenter(node);
    }

    @FXML
    private void onItems() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/items.fxml"));
        loader.setControllerFactory(applicationContext::getBean);
        Parent node = loader.load();
        root.setCenter(node);
    }

    @FXML
    private void onLoggingHistory() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loggingHistory.fxml"));
        loader.setControllerFactory(applicationContext::getBean);
        Parent node = loader.load();
        root.setCenter(node);
    }
}
