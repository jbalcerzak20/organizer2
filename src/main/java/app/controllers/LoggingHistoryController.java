package app.controllers;

import app.entities.LogPerson;
import app.entities.Person;
import app.repositories.PersonRepository;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ResourceBundle;

@Controller
public class LoggingHistoryController implements Initializable{

    @Autowired
    private ApplicationContext applicationContext;
    @FXML
    private TableView table;
    @FXML
    private AnchorPane root;
    @Autowired
    private MainController mainController;
    @Autowired
    private PersonRepository personRepository;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        TableColumn<LogPerson, Timestamp> login = new TableColumn<>("Logowanie");
        TableColumn<LogPerson, Timestamp> logout = new TableColumn<>("Wylogowanie");
        login.setCellValueFactory(new PropertyValueFactory<>("login"));
        logout.setCellValueFactory(new PropertyValueFactory<>("logout"));

//        login.setCellFactory(column -> {
//            TableCell<LogPerson, Date> cell = new TableCell<LogPerson, Date>() {
//                @Override
//                protected void updateItem(Date item, boolean empty) {
//                    super.updateItem(item, empty);
//                    if (empty) {
//                        setText(null);
//                    } else {
//                        setText(item.toString());
//                    }
//                }
//            };
//            return cell;
//        });

        table.getColumns().addAll(login,logout);

        Person person = personRepository.findOne(mainController.getCurrentUserId());
        table.getItems().addAll(person.getLoggers());
    }
}
