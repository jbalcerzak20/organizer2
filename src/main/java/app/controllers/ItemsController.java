package app.controllers;

import app.entities.Item;
import app.repositories.ItemRepository;
import app.repositories.PersonRepository;
import app.services.SecureService;
import com.sun.javafx.application.HostServicesDelegate;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Controller
public class ItemsController implements Initializable {

    @FXML
    @Getter
    private TableView<Item> table;
    @FXML
    private TextField login;
    @FXML
    private PasswordField password;
    @FXML
    private TextArea description;
    @FXML
    private TextField email;
    @FXML
    private TextField link;
    @FXML
    private TextField searchedText;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private SecureService secureService;
    @Autowired
    private HostServicesDelegate hostServicesDelegate;
    private Item currentProgressItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TableColumn<Item, Integer> id = new TableColumn<>();
        TableColumn<Item, String> login = new TableColumn<>();
        TableColumn<Item, String> password = new TableColumn<>();
        TableColumn<Item, String> description = new TableColumn<>();
        TableColumn<Item, String> email = new TableColumn<>();
        TableColumn<Item, String> link = new TableColumn<>();
        TableColumn edit = new TableColumn<>();
        id.setText("Id");
        login.setText("Login");
        password.setText("Hasło");
        description.setText("Opis");
        email.setText("Email");
        link.setText("Strona");
        edit.setText("Edycja");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        login.setCellValueFactory(new PropertyValueFactory<>("loginDecode"));
        password.setCellValueFactory(new PropertyValueFactory<>("passwordDecode"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        link.setCellValueFactory(new PropertyValueFactory<>("link"));
        edit.setCellValueFactory(new PropertyValueFactory<>(""));

        edit.setCellFactory(prepareTableCellOptions());

        table.setRowFactory(new Callback<TableView<Item>, TableRow<Item>>() {
            @Override
            public TableRow<Item> call(TableView<Item> param) {
                TableRow<Item> itemTableRow = new TableRow<>();

                itemTableRow.setOnMouseClicked(event -> {
                    if(event.getClickCount() == 2 && !itemTableRow.isEmpty()){
                        Item item = itemTableRow.getItem();
                    hostServicesDelegate.showDocument(item.getLink());
                    }
                });

                return itemTableRow;
            }
        });

        table.getColumns().addAll(id, login, password, description, email, edit);
        itemRepository.findAllByOrderByLogin().forEach(item -> {
            item.initDecodeText(secureService);
            table.getItems().add(item);
        });
        clearInsertOrUpdateForm();

        table.setPrefHeight(600);

    }

    private void setProgressItem(Item item) {
        currentProgressItem = item;
        login.setText(secureService.decodeAES(item.getLogin()));
        password.setText(secureService.decodeAES(item.getPassword()));
        email.setText(item.getEmail());
        link.setText(item.getLink());
        description.setText(item.getDescription());
    }

    @FXML
    public void onSaveItem() {
        currentProgressItem.setLogin(secureService.encodeAES(login.getText()));
        currentProgressItem.setPassword(secureService.encodeAES(password.getText()));
        currentProgressItem.setDescription(description.getText());
        currentProgressItem.setEmail(email.getText());
        currentProgressItem.setLink(link.getText());
        itemRepository.save(currentProgressItem);

        clearInsertOrUpdateForm();
        table.getItems().clear();
        itemRepository.findAllByOrderByLogin().forEach(it -> {
            it.initDecodeText(secureService);
            table.getItems().add(it);
        });
    }

    private void clearInsertOrUpdateForm() {
        login.setText("");
        password.setText("");
        description.setText("");
        email.setText("");
        link.setText("");
        currentProgressItem = new Item();
    }

    @FXML
    private void onSearch() {
        if (!StringUtils.isEmpty(searchedText.getText())) {
            List<Item> filteredList = table.getItems().stream().filter(it -> {
                return ((StringUtils.isNotBlank(it.getDescription()) && it.getDescription().toLowerCase().contains(searchedText.getText().toLowerCase()))
                        || (StringUtils.isNotBlank(it.getLoginDecode()) && it.getLoginDecode().toLowerCase().contains(searchedText.getText().toLowerCase())));

            }).collect(Collectors.toList());
            table.getItems().clear();
            table.getItems().addAll(filteredList);
        } else {
            table.getItems().clear();
            itemRepository.findAllByOrderByLogin().forEach(it -> {
                it.initDecodeText(secureService);
                table.getItems().add(it);
            });
        }
    }

    @FXML
    private void onCancelItem() {
        clearInsertOrUpdateForm();
    }

    private Callback<TableColumn<Item, Void>, TableCell<Item, Void>> prepareTableCellOptions() {
        return new Callback<TableColumn<Item, Void>, TableCell<Item, Void>>() {
            @Override
            public TableCell<Item, Void> call(TableColumn<Item, Void> param) {

                TableCell<Item, Void> tableCell = new TableCell<Item, Void>() {
                    Button btn = new Button("Edytuj");
                    Button deleteBtn = new Button("Usuń");
                    HBox hBox = new HBox();

                    {
                        btn.setOnAction(event -> {
                            Item item = getTableView().getItems().get(getIndex());
                            setProgressItem(item);
                        });

                        deleteBtn.setOnAction(event -> {

                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Usuwanie");
                            alert.setContentText("Czy na pewno usunac eleemnt?");

                            if(alert.showAndWait().get().equals(ButtonType.OK)) {
                                Item item = getTableView().getItems().get(getIndex());
                                itemRepository.delete(item);
                                getTableView().getItems().remove(getIndex());
                            }
                        });

                        hBox.getChildren().addAll(btn, deleteBtn);
                    }

                    @Override
                    protected void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(hBox);
                        }
                    }
                };
                return tableCell;
            }
        };
    }

}
