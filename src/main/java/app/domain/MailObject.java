package app.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailObject {
    private String from;
    private String to;
    private String content;
    private String subject;
}
