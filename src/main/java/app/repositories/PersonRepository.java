package app.repositories;

import app.entities.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person,Long> {

   Person findByLogin(String login);

   Person findByEmail(String email);

}
