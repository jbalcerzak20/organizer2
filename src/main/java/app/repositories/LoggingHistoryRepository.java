package app.repositories;

import app.entities.LogPerson;
import org.springframework.data.repository.CrudRepository;

public interface LoggingHistoryRepository extends CrudRepository<LogPerson, Long> {
    LogPerson findTop1ByLogoutIsNull();
}
