package app.repositories;

import app.entities.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Long> {

    List<Item> findByLoginContainingOrDescriptionContaining(String login, String description);

    List<Item> findAllByOrderByLogin();
}
